# Hintergründiges

## Make

Vielleicht kennt der eine oder andere von euch das Tool `make`. Damit lässt sich recht einfach ein Build-Prozess für eine Software aufsetzen, ohne Server und mit mäßigem Aufwand. Man muss natürlich die Syntax kennenlernen.

[Make](https://www.gnu.org/software/make/) bekommt über ein sog. "Makefile" Informationen über die Abhängigkeiten der einzelnen Dateien voneinander, und mit welchen Befehlen sie bei Bedarf erzeugt werden. Wie auch immer das Projekt aussieht: Wenn man die Abhängigkeiten einmal definiert hat, reicht ein einfaches Kommando `make`, und alles baut sich wie von Zauberhand zusammen.

## Tests

Einige von uns beschäftigen sich ja mit automatisierten Testverfahren, [sogar in Access](http://wiki.access-codelib.net/AccUnit)! Falls es sich nicht nur um Unit-Tests, sondern sogar um GUI- oder Integrations-Tests handelt, ist es notwendig, dass die Applikation in einer "ausführbaren" Form vorliegt. Damit meine ich nicht das alte Problem, wie man aus einer Access-Datenbank eine EXE-Datei macht :-)

Es geht einfach darum, die verschiedenen Aktionen, die ich vor dem *Deployment* machen muss, zu automatisieren. Und wenigstens für die Bereitstellung der Testversion für interessierte Anwender hilft mir das, mich auf das Wesentliche zu konzentrieren: Die Entwicklung. Wenn ich dann noch ein Continuous-Integration-System wie z.B. [Jenkins](https://www.juengling-edv.de/jenkins/) oder Bitbuckets [Pipelines](https://bitbucket.org/product/features/pipelines) (leider nicht für Windows) zur Verfügung habe, spare ich eine Menge Zeit.

Was ich mit Make alles machen kann ist zwar schon sehr schön, aber die Grenze des Erreichbaren ist dabei immer die Kommandozeile. Gerade bei Access ist dabei die Frage wichtig, ob ich wirklich alles mit von der Kommandozeile ausführbaren Tools machen kann.


## Continous Integration

Die Grundidee solcher Automatisierung ist keineswegs neu; sie heißt "Continous Integration", oder abgekürzt "CI". 

- Allgemeines zu CI
- Grundsätzlicher Gedanke
- Typischer Ablauf


# CI und Access

## Wie kriegen wir CI in Access rein?

Für unser Access-Projekt geht es jetzt erstmal darum, die notwendigen Schritte vor der Auslieferung zu identifizieren und einzeln zu automatisieren. Gehen wir die oben aufgeführten Schritte mal einzeln durch. Es mag durchaus Abweichungen je nach Projekt geben, aber das ist zunächst nicht weiter von Bedeutung. Wenn wir das Grundgerüst haben, dann sind kleinere Anpassungen hoffentlich möglich.


### Testdaten entfernen

Immer wenn ich eine Datenbankapplikation entwickle, werde ich hin und wieder Daten erfassen, um ein Formular oder einen Bericht auszuprobieren. Was während der Entwicklung sogar gewünscht ist, stört bei der Auslieferung massiv. Besonders wenn in der Betaphase auch das Backend mit ausgeliefert wird.

Testdaten zu entfernen ist im Grunde recht einfach: Man muss einfach ein paar SQL-DELETE-Befehle in der richtigen Reihenfolge (wegen der referentiellen Integrität) ausführen. Das sollte kein großes Problem sein.

Doch inzwischen habe ich noch einen anderen Weg gefunden.


### Access-Datenbank aus der Quellcodeverwaltung mit OASIS aufbauen

Statt die bestehende Datenbank zu bereinigen, erstellen wir sie einfach frisch von der Quellcodeverwaltung. OASIS kann das mit einem ganz einfachen Befehl, sogar in einem Schritt bis zuer .accde:

`msaccess.exe /cmd OASIS#D:\MeinProjekt\Verzeichnis\MeineDatei.accde`

Mit dem kompletten Pfad zur Access-EXE vorneweg und der Voraussetzung, dass dort, wo die .accde stehen soll, eine gleichnamige .oasis-Datei liegt, ist das also in einem Schritt erledigt.


### Aktuellen Code beschaffen

Vorneweg muss lediglich noch der gewünschte Branch ausgecheckt werden, z.B. mit Git in ein frisches Verzeichnis (wenn es sich um "master" handelt):

`git clone REPOSITORYURL`


### Versionsnummer in Applikation und Setup-Script aktualisieren

Nun kann noch im Sourcecode und im Setup-Script die Versionsnummer, die Buildnummer und/oder der Hashcode in die entsprechenden Konstanten gepatcht werden. Das geht mit einem Tool deiner Wahl (z.B. Python), oder wir bauen entsprechendes einfach in unser AccessMake ein. Im Grunde nur ein wenig Stringerarbeitung in Textdateien, so schwer ist das nicht.


### Diese letzten Änderungen in der Quellcodeverwaltung committen

`git add .`
`git commit -m "Automatischer Commit"`

Einfacher geht es kaum.


### Setup laufen lassen

Das ist bei InnoSetup z.B. auch nur ein `iscc.exe ...`-Befehl.


### Hochladen zum Kunden

Hier könnte z.B. mit WinScp oder ähnlichem gearbeitet werden, je nachdem wohin die Datei geschickt werden soll.

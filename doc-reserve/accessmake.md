% **AccessMake** (Arbeitstitel)
% Christoph Jüngling, [www.juengling-edv.de](https://www.juengling-edv.de)
% Dezember 2017

\newpage

# Wie verwende ich AccessMake?

## Download und Installation

Die aktuellste Version befindet sich immer in dem Eintrag mit der höchsten
Versionsnummer auf der [Website des Projektes](https://gitlab.com/juengling/AccessMake/tags). 

Zwischenversionen während der Entwicklung sind jeweils eine Woche lang in den
[Pipelines](https://gitlab.com/juengling/AccessMake/pipelines) verfügbar. Diese
Versionen sind jedoch mit Vorsicht zu verwenden, da kann schon mal was bei
schiefgehen ...

## Hilfe

AccessMake ist ein reines Kommandozeilenprogramm. Wie bei vielen Programmen
kann man einen ersten Eindruck von den Fähigkeiten bekommen, indem man auf der
Kommandozeile folgenden Befehl eingibt:

    amake --help

Dies ergibt folgende Ausgabe:

	Access Make v0.7.0
	usage: Access Make [-h] [-v] [config]

	Help the MS Access developer (and others) to create a build environment for
	continuous integration.

	positional arguments:
	  config         Name/path of the configuration file. Default is "amakefile"
					 in the current folder.

	optional arguments:
	  -h, --help     show this help message and exit
	  -v, --verbose  Increase verbosity level

Nun wissen wir, dass als erstes Argument ein Konfigurationsfile angegeben
werden kann. Wird dies nicht angegeben, geht das Programm davon aus, dass
es ein `amakefile` im aktuellen Arbeitsverzeichnis gibt.

Alle Pfadangaben beziehen sich jeweils relativ auf den Standort dieser Datei,
egal ob der Pfad dorthin nun angegeben wurde oder nicht. Dadurch müssen in der
Konfiguratonsdatei keine absoluten Pfade verwendet werden und der Befehl sollte
in allen Verzeichnissen funktionieren, egal wohin das Projekt geclont wird.

## Das AMakefile

Eine Zeile im AMakefile kann sein:

* Leerzeile
* Eine Kommentarzeile mit einem # am Anfang
* Eine Zeile mit einem Befehl

Momentan werden folgende Befehle unterstützt:

* read
* write
* get

### Der READ-Befehl

Der READ-Befehl liest aus einer angegebenen Datei einen Wert. Die Befehlszeile
ist folgendermaßen aufgebaut:  

    READ dateiname USING regex 

### Der WRITE-Befehl

### Der GET-Befehl


# Wie alles begann ...

##Motivation

Immer wenn ich mal wieder ein Access-Projekt habe, stellt sich mir das gleiche Problem: Die Aktionen, die ich vor dem *Deployment* machen muss, nerven mich gewaltig. Jedesmal das gleiche Spiel:

- Testdaten entfernen
- Versionsnummer in Applikation und Setup-Script aktualisieren
- Alles mit OASIS exportieren
- Diese letzten Änderungen in der Quellcodeverwaltung committen
- Datenbank komprimieren oder .accde erstellen
- Setup laufen lassen
- Hochladen in die Cloud

Hinzu kommt das Problem, dass ich irgendwie gern sicherstellen würde, dass mein Code in der Quellcodeverwaltung auch tatsächlich mit dem übereinstimmt, den ich während der Entwicklung erstellt habe. Denn wenn ich zufälligerweise vergessen haben sollte, diese eine wichtige Änderung einzuchecken, dann kann ich mir später einen Wolf suchen, wenn der Kunde einen Bug meldet, den ich in *meinem* Code nicht nachvollziehen kann.

Wenn ich diese Schritte automatisieren könnte, wäre mir eine große Last von den Schultern genommen. Und wenn ich dabei sicherstellen könnte, dass der *Build* tatsächlich aus der Quellcodeverwaltung heraus erfolgt, dann wäre ich doppelt beruhigt.

Auch wenn ich die Abschlussarbeiten für das *Final Release* vielleicht doch von Hand durchführe: Zumindest für die häufigere Bereitstellung der Testversion für interessierte Anwender hilft mir das, mich auf das Wesentliche zu konzentrieren: Die Entwicklung.

So entstand die Idee für ein **Access-Make** (der Name ist momentan noch ein Arbeitstitel).

## Im Laufe der Zeit

>"I am so clever that sometimes I don't understand a single word of what I am saying." - Oscar Wilde


So ging es mir im Verlauf dieses Projektes mehrfach. Irgendwie habe ich immer zwischen den Stühlen gesessen. Sollte ich *AccessMake* mit Access oder mit Python programmieren? Python kann eleganter mit Stringmanipulationen umgehen, Access ist gerade als Unterstützung bei Access-nahen Aktionen irgendwie "naheliegender". 

Die im vorigen Kapitel geschilderte ursprüngliche Idee hat also im Laufe der Zeit einige Änderungen erfahren. Inzwischen bin ich wieder bei Python angelangt, und das hatte folgende Gründe.

Zum Einen habe ich in [GitLab](https://gitlab.com) ein CI-System gefunden, das Windows unterstützt. Es tut dies in der Form, dass ein sog. "Runner" unter Windows läuft. Dieser nimmt die Aufträge des CI-Systems entgegen und führt sie aus. Dadurch fielen einige organisatorische Arbeiten weg, die nun von Gitlabs "Pipelines" übernommen werden.

Dann habe ich auf der [AEK20](http://www.donkarl.com/AEK/Archiv/AEK20.htm) mit Bernd Gilles, dem Entwickler von OASIS, gesprochen. Dieser erklärte mir, dass OASIS mittels eines einfachen Kommandozeilenbefehls direkt aus dem Quellcode eine .accdb oder .accde erstellen kann. Damit fielen auch diese Aktionen weg, die in Python etwas instabil liefen.

In Anbetracht der direkten Erzeugung der .accde aus dem Quellcode war dann auch die Ausführung von Löschcode für die Testdaten nicht mehr notwendig. Was gar nicht erst in Git enthalten ist, wird auch nicht in die .accde-Datei kommen und braucht daher auch nicht gelöscht zu werden.

Und letztlich konnte auch das "Hochladen zum Kunden" von dem CI-System übernommen werden, es nennt sich dort "collect artifacts".

Damit blieb vor dem Aufbau der .accde-Datei und dem Compilieren des Setups nur noch ein wenig Textverarbeitung übrig:

- Versionsnummer aus dem Quellcode auslesen und in Setup-Script patchen
- aktuellen Git-Hash ermitteln und in Quellcode und Setup-Script patchen
- aktuelles Datum ermitteln, als ISO-Date formatieren und in Quellcode und Setup-Script patchen

Und dafür ist Python hervorragend geeignet.

'''
Created on 12.12.2017

@author: christoph.juengling
'''
import unittest

from amake import parse_commands
import publics


class TestGrammar(unittest.TestCase):

    def setUp(self):
        # Cannot test this, but it makes the coverage a bit better :-)
        publics.verbose = 3

    def test_read(self):
        commands = parse_commands(
            ['''READ src\\amake.py USING 'version': '([0-9.]*)'''])
        self.assertEqual(commands[0][0], 'read-using', 'Wrong command')
        self.assertEqual(commands[0][1], r'src\amake.py', 'Wrong file')
        self.assertEqual(
            commands[0][2], "'version': '([0-9.]*)", 'Wrong regex')

    def test_read_VB6(self):
        commands = parse_commands(
            ['''READ src\\myproject.vbp MACRO vb6version'''])
        print(commands)
        self.assertEqual(commands[0][0], 'read-macro', 'Wrong command')
        self.assertEqual(commands[0][1], r'src\myproject.vbp', 'Wrong file')
        self.assertEqual(commands[0][2], "vb6version", 'Wrong macro')
        #info = perform_commands('', commands)

    def test_write(self):
        commands = parse_commands(
            ['''WRITE build.cmd USING .*"([0-9.]*)".*'''])
        self.assertEqual(commands[0][0], 'write', 'Wrong command')
        self.assertEqual(commands[0][1], 'build.cmd', 'Wrong file')
        self.assertEqual(
            commands[0][2], '.*"([0-9.]*)".*', 'Wrong regex')

    def test_getIsodate(self):
        commands = parse_commands(['GET isodate'])
        self.assertEqual(commands[0][0], 'get', 'Wrong command')
        self.assertEqual(commands[0][1], 'isodate', 'Wrong information')

    def test_getNothing(self):
        commands = parse_commands(['GET nothing'])
        self.assertEqual(commands[0][0], 'get', 'Wrong command')
        self.assertEqual(commands[0][1], 'nothing', 'Wrong information')

    def test_getisodate_value(self):
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

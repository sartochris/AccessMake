Wie verwende ich AccessMake?
============================

Hilfe
-----

.. highlight:: none

AccessMake ist ein reines Kommandozeilenprogramm. Wie bei vielen
Programmen kann man einen ersten Eindruck von den Fähigkeiten bekommen,
indem man auf der Kommandozeile folgenden Befehl eingibt::

   amake --help

Dies ergibt folgende Ausgabe:

::

    Access Make v0.8.9
    usage: amake [-h] [-v] [-n] [--version] [config]

    Help the MS Access developer (and others) to create a build environment for
    continuous integration. See the documentation web site for help instructions
    (currently in German only) at https://juengling.gitlab.io/AccessMake-Doc/.
    Please file bug reports or feature requests in the project's issue tracker at
    https://gitlab.com/juengling/AccessMake/issues.

    positional arguments:
      config                Name/path of the configuration file. Default is
                            "amakefile" in the current folder.

    optional arguments:
      -h, --help            show this help message and exit
      -v, --verbose         Increase verbosity level
      -n, --no-action, --dry-run
                            Perform no changes, just print what you were doing
      --version             Print program name and version number, then exit

Nun wissen wir, dass als erstes Argument ein Konfigurationsfile
angegeben werden kann. Wird dies nicht angegeben, geht das Programm
davon aus, dass es ein ``amakefile`` im aktuellen Arbeitsverzeichnis
gibt.

Alle Pfadangaben beziehen sich jeweils relativ auf den Standort dieser
Datei, egal ob der Pfad dorthin nun angegeben wurde oder nicht. Dadurch
müssen in der Konfigurationsdatei keine absoluten Pfade verwendet werden
und der Befehl sollte in allen Verzeichnissen funktionieren, egal wohin
das Projekt geclont wird.

Allgemeines
-----------

Eine Zeile im AMakefile kann sein:

-  Leerzeile
-  Eine Kommentarzeile mit einem # am Anfang
-  Eine Zeile mit einem Befehl

Momentan werden folgende Befehle unterstützt:

-  read
-  write
-  get

Ein **READ** oder **GET** liest eine bestimmte Information ein
und speichert sie. Jeder nachfolgende **WRITE** schreibt diese
Information dann in die angegebene Datei. READ und WRITE erwarten
ein Regex, der GET-Befehl funktioniert etwas anders.

Die Befehle werden in dieser Dokumentation zur besseren Wiedererkennung
in Großbuchstaben geschrieben. Im tatsächlichen AMakefile sind jedoch
auch kleine Buchstaben erlaubt.


.. highlight:: none

Der READ-Befehl
---------------

Der READ-Befehl liest aus einer angegebenen Datei einen Wert. Die
Befehlszeile ist folgendermaßen aufgebaut::

   READ dateiname USING regex 

Dies ist zu lesen als: *"Lies aus der angegebenen Datei <dateiname>
die Information unter Verwendung des Regex <regex>"*.

Als Regex (regular expression) können die üblichen Anweisungen verwendet werden.
Eine recht umfangreiche Zusammenstellung findet man auf
`dieser Website <https://www.regular-expressions.info/>`_.

Es muss jedoch eine "Gruppe" vorhanden sein, die den Teil der
matchenden Zeile umfasst, der gelesen werden soll::

    READ src\amake.py USING 'version': '([0-9.]*)'

In diesem Beispiel wird aus dem Sourcecode von ``amake.py``
die Deklaration der Version in dem dict PROGINFO gesucht.
Es beginnt mit dem Wort "version" in Apostrophs, danach kommt ein Doppelpunkt
und ein Leerzeichen, dann wieder ein Apostroph. Jetzt beginnt der eigentliche
Ausdruck, wiederum durch ein Apostroph abgeschlossen.

Der zu lesende Ausdruck besteht aus einer beliebig häufigen Anzahl an Ziffern
und Punkten.

.. highlight:: python

Aktuell sieht die Zeile im Source z.B. so aus::

    'version': '0.8.9',

Mit dem obigen READ-Befehl wird also nur die Zeichenfolge "0.8.9" ausgelesen
und für die weitere Verarbeitung zwischengespeichert.

.. highlight:: none

Der WRITE-Befehl
----------------

Der WRITE-Befehl schreibt eine zuvor per READ gelesene oder per GET ermittelte
Information in eine Datei, wiederum unter Verwendung eines Regex::

    WRITE setup\setup-w7.iss USING #define MyAppVersion "([0-9.]*)"

Diesmal wird die Datei "setup-w7.iss" (ein Inno-Setup-File) aus dem Verzeichnis
"setup" beschrieben. Der Regex matcht eine Definition im Script, die im Source
so aussieht::

    #define MyAppVersion "0.0.0"

Durch die initiale Angabe "0.0.0" wird das Setup diese falsche Versionsnummer
anzeigen, falls AMake nicht gelaufen ist oder nicht richtig gearbeitet hat.
Da im Zuge des Build-Prozesses kein Commit erfolgt, sieht  die Versionsnummer
im Repository immer genau so aus.

Auch hier kennzeichnet die Gruppe im Regex (durch die runden Klammern eingefasst)
das Muster, das durch die zuvor gespeicherte Information zu ersetzen ist. Dies
geschieht in AMake durch einen einfachen ``replace``.

	
Der GET-Befehl
--------------

Der GET-Befehl kann im Moment nur das Datum beschaffen::

    GET isodate

Da der Begriff "isodate" hier festgelegt ist, ist kein Regex
erforderlich. Es wird das Systemdatum mit der folgenden Python-Funktion
ermittelt::

    datetime.today().strftime('%Y-%m-%d')


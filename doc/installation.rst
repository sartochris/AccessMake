Download und Installation
=========================

Die aktuellste Version befindet sich immer in dem Eintrag mit der
höchsten Versionsnummer auf der `Website des
Projektes <https://gitlab.com/juengling/AccessMake/tags>`_.

Zwischenversionen während der Entwicklung sind jeweils eine Woche lang
in den `Pipelines <https://gitlab.com/juengling/AccessMake/pipelines>`_
verfügbar. Diese Versionen sind jedoch mit Vorsicht zu verwenden, da
kann schon mal was bei schiefgehen ...

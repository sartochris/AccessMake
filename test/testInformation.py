'''
Created on 22.09.2018

@author: Christoph Juengling
'''

#pylint: disable=missing-class-docstring, missing-function-docstring

from os import getcwd
import unittest

from amake import parse_commands, perform_commands


class TestInformation(unittest.TestCase):

    def test_get_isodate(self):
        commands = parse_commands(['GET isodate'])
        info = perform_commands(getcwd(), commands)
        self.assertEqual(len(info), 10, 'Wrong length')
        self.assertEqual(info.count('-'), 2)

    def test_get_isodatetime(self):
        commands = parse_commands(['GET isodatetime'])
        info = perform_commands(getcwd(), commands)
        self.assertEqual(len(info), 19, 'Wrong length')
        self.assertEqual(info.count('-'), 2)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

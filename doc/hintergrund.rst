Begriffe
========

Make
----

Vielleicht kennt der eine oder andere von euch das Tool ``make``. Damit
lässt sich recht einfach ein Build-Prozess für eine Software aufsetzen,
ohne Server und mit mäßigem Aufwand. Man muss natürlich die Syntax
kennenlernen.

`Make <https://www.gnu.org/software/make/>`_ bekommt über ein sog.
"Makefile" Informationen über die Abhängigkeiten der einzelnen Dateien
voneinander, und mit welchen Befehlen sie bei Bedarf erzeugt werden. Wie
auch immer das Projekt aussieht: Wenn man die Abhängigkeiten einmal
definiert hat, reicht ein einfaches Kommando ``make``, und alles baut
sich wie von Zauberhand zusammen.

Allerdings stellt sich die Frage, wie lange der Buildprozess im schlimmsten Falle läuft.
Wenn ich wirklich alles aufbauen muss und keine Zwischenergebnisse nutzen kann,
und das läuft dann allerhöchstens eine Minute, dann brauche ich mir eigentlich
keine Gedanken über ein Makefile zu machen, dann reicht ein simpler Batch
ebensogut.

Auch die Frage, ob Make überhaupt angemessen ist, stellt sich hier. Da die
Entscheidung, ob ein Compilerlauf gemacht werden muss oder nicht, durch Make
von dem Dateidatum und der -uhrzeit abhängig gemacht wird, wird das nach einem
Checkout möglicherweise gar nicht richtig funktionieren. Welche Quellcodeverwaltung
stellt auch Datum und Uhrzeit einer Datei wieder her?

Aus diesem Grunde verwende ich für meine CI kein Gnu-Make mehr.


Tests
-----

Einige von uns beschäftigen sich ja mit automatisierten Testverfahren,
`sogar in Access <http://wiki.access-codelib.net/AccUnit>`_! Falls es
sich nicht nur um Unit-Tests, sondern sogar um GUI- oder
Integrations-Tests handelt, ist es notwendig, dass die Applikation in
einer "ausführbaren" Form vorliegt. Damit meine ich nicht das alte
Problem, wie man aus einer Access-Datenbank eine EXE-Datei macht :-)
sondern die Form, in der auch die Anwender damit arbeiten würden.


Continous Integration
---------------------

Bei "Continous Integration" (CI) geht es einfach darum, die verschiedenen Aktionen,
die ich vor dem *Deployment* machen muss, zu automatisieren.

Und wenigstens für die
Bereitstellung der Testversion für interessierte Anwender hilft mir das,
mich auf das Wesentliche zu konzentrieren: Die Entwicklung.

Wenn ich dann noch ein Continuous-Integration-System wie z.B.
`Jenkins <https://www.juengling-edv.de/jenkins/>`_, Bitbuckets
`Pipelines <https://bitbucket.org/product/features/pipelines>`_ (leider
nicht für Windows) oder Gitlabs
`Continuous Integration & Deployment <https://about.gitlab.com/features/gitlab-ci-cd/>`_
zur Verfügung habe, spare ich eine Menge Zeit.

Die Grundidee solcher Automatisierung ist keineswegs neu; sie heißt
"Continous Integration", oder abgekürzt "CI".

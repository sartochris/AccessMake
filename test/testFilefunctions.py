'''
Created on 22.09.2018

@author: Christoph Juengling
'''
from io import StringIO
import unittest

from amake import read_file, get_content_by_macro, get_content, get_info
import publics


class TestFileFunctions(unittest.TestCase):

    def setUp(self):
        # Cannot test this, but it makes the coverage a bit better :-)
        publics.verbose = 1

    def testReadfile_empty(self):
        file = StringIO()
        content = read_file(file)
        self.assertEqual(len(content), 1)

    def testReadfile_content(self):
        file = StringIO('''READ src\amake.py USING 'version': '([0-9.]*)'
WRITE build-app.cmd USING .*"([0-9.]*)".*
WRITE setup\setup-w7.iss USING #define MyAppVersion "([0-9.]*)"
WRITE setup\setup-xp.iss USING #define MyAppVersion "([0-9.]*)"
WRITE doc\conf.py USING release = '([0-9.]*)'

READ hash USING ([0-9A-F]{7,})
WRITE setup\setup-w7.iss USING #define +MyAppHash +"([0-9A-F]*)"
WRITE setup\setup-xp.iss USING #define +MyAppHash +"([0-9A-F]*)"

GET isodate
WRITE setup\setup-w7.iss USING #define +MyAppDate +"([0-9.-]*)"
WRITE setup\setup-xp.iss USING #define +MyAppDate +"([0-9.-]*)"

GET nothing
WRITE project.prj USING CondComp=(.*)
''')
        content = read_file(file)

        # Function always returns one extra empty line at the end of the file
        # if there is an ending line feed
        self.assertEqual(len(content), 17)

    def test_getcontent_emptyfile_emptyregex(self):
        file = StringIO()
        regex = ''
        actual = get_content(file, regex)
        self.assertEqual(actual, '')

    def test_getcontent_emptyfile_regex(self):
        file = StringIO()
        regex = 'bla-blubb'
        actual = get_content(file, regex)
        self.assertEqual(actual, '')

    def test_getcontent_file_regex(self):
        file = StringIO('''PROGINFO = {
    'version': '1.2.3',
    'name': 'Access Make',
    'shortname': 'amake',
    'title': 'Access Make'
}
''')
        regex = "'version': '([0-9.]*)'"
        actual = get_content(file, regex)
        self.assertEqual(actual, '1.2.3')

    def test_getcontentbymacro_vb6version(self):
        file = StringIO('''MajorVer=1
MinorVer=2
RevisionVer=3
''')
        macro = 'vb6version'
        actual = get_content_by_macro(file, macro)
        self.assertEqual(actual, '1.2.3')

    def test_getcontentbymacro_unknownmacro(self):
        file = StringIO('''MajorVer=1
MinorVer=2
RevisionVer=3
''')
        macro = 'dummy'
        actual = get_content_by_macro(file, macro)
        self.assertIsNone(actual)

    def test_getinfo_macro_vb6version(self):
        file = StringIO('''MajorVer=1
MinorVer=2
RevisionVer=3
''')
        actual = get_info('Test', file, macro='vb6version')
        self.assertEqual(actual, '1.2.3')

    def test_getinfo_macro_unknown(self):
        file = StringIO('''MajorVer=1
MinorVer=2
RevisionVer=3
''')
        actual = get_info('Test', file, macro='dummy')
        self.assertIsNone(actual)

    def test_getinfo_regex(self):
        file = StringIO('''PROGINFO = {
    'version': '1.2.3',
    'name': 'Access Make',
    'shortname': 'amake',
    'title': 'Access Make'
}
''')
        regex = "'version': '([0-9.]*)'"
        actual = get_info('Test', file, regex=regex)
        self.assertEqual(actual, '1.2.3')


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testReadfile']
    unittest.main()

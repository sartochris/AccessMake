'''
Created on 08.08.2017

@author: Christoph Juengling <chris@juengling-edv.de>
'''
from argparse import ArgumentParser
from datetime import datetime
from os import getcwd
from os.path import dirname, join as pjoin
import fileinput
import logging
import re
import sys
import traceback

import publics
import pyparsing as pp

__appname__ = 'Access Make'
__version__ = '0.8.10'
__description__ = '''Help the MS Access developer
    (and others) to create a build environment for
    continuous integration.

    See the documentation web site for help instructions (currently in German only)
    at https://juengling.gitlab.io/AccessMake-Doc/.

    Please file bug reports
    or feature requests in the project\'s issue tracker at
    https://gitlab.com/juengling/AccessMake/issues.
    '''


def main(argv):
    '''
    Main program

    :param argv: Command line arguments
    :return: Command line status (Windows: "errorlevel")
    :rtype: Positive integer or 0
    '''

    # To become part of the build log, print the same info
    # from --version command line argument
    print(name_and_version())

    args = parse_command_line(argv)
    if args.version:
        # Name and version already printed
        return 0

    publics.verbose = args.verbose
    publics.noaction = args.no_action

    try:
        basefolder = get_basefolder(args.config)

        with open(args.config) as file:
            cmdfile_content = read_file(file)

        commands = parse_commands(cmdfile_content)
        perform_commands(basefolder, commands)

    except FileNotFoundError:
        return 1

    except Exception:  # pylint: disable=broad-except
        traceback.print_exc()
        return 1

    return 0


def parse_command_line(arguments):
    '''
    Parse the command line arguments

    :param arguments: List of command line arguments
    :return: Arguments object
    '''
    parser = ArgumentParser(prog=__appname__,
                            description=__description__)

    parser.add_argument('config', nargs='?', default='amakefile',
                        help='Name/path of the configuration file. Default is "amakefile"'
                        ' in the current folder.')

    parser.add_argument('-v', '--verbose',
                        action='count',
                        default=0,
                        help='Increase verbosity level')

    parser.add_argument('-n', '--no-action', '--dry-run',
                        action='store_true',
                        help='Perform no changes, just print what you were doing')

    parser.add_argument('--version',
                        action='store_true',
                        help='Print program name and version number, then exit')

    args = parser.parse_args(arguments)
    return args


def name_and_version():
    '''
    Compile name and version and return

    :return: Program's name and version in a usual way
    :rtype: str
    '''
    return '{pgm} v{version}'.format(
        pgm=__appname__,
        version=__version__)


def get_content(file, regex):
    '''
    Retrieve information from <file> by <regex>

    :param File file:  File object to retrieve information from
    :param str regex: Regular expression (first match will be used as result)
    '''
    result = None

    pattern = re.compile(regex, re.IGNORECASE)  #@UndefinedVariable
    for line in file:
        r = pattern.search(line)
        if r:
            result = r.group(1)
            break

    return result or ''


def get_content_by_macro(file, macro):
    '''
    Retrieve information from <file> by <macro>

    For macro "vb6version":
        MajorVer=1
        MinorVer=2
        RevisionVer=3

    :param File file:  File object to retrieve information from
    :param str macro: Name of the macro command to perform
    '''
    if macro == 'vb6version':
        majorver = minorver = revision = ''

        for line in file:
            if line.startswith('MajorVer'):
                majorver = line.split('=')[1].rstrip()

            if line.startswith('MinorVer'):
                minorver = line.split('=')[1].rstrip()

            if line.startswith('RevisionVer'):
                revision = line.split('=')[1].rstrip()

        return '.'.join([majorver, minorver, revision])


def write_content(filename, regex, value):
    '''
    Write information <value> into <filename>, where the position is identified by <regex>

    :param str filename:  Name and path of the file to retrieve information from
    :param str regex:     Regular expression (first match will be used as result)
    :param str value:     The value (string) to write
    '''

    prog = re.compile(regex, re.IGNORECASE)  #@UndefinedVariable

    protocol = []

    if publics.noaction:
        with open(filename) as file:
            content = file.read().splitlines()

        lineno = 0
        for line in content:
            lineno += 1
            match = prog.match(line)
            if match:
                oldvalue = match.group(1)
                print('Replace "{}" by "{}" in file {} line {}'.format(
                    oldvalue, value, filename, lineno))
    else:
        for line in fileinput.input(filename, inplace=True):
            match = prog.match(line)
            if match:
                # Get first matching group's value
                oldvalue = match.group(1)

                if publics.verbose >= 2:
                    protocol.append('Replace "{}" by "{}" in file {}'.format(
                        oldvalue, value, filename))

                # Replace old string with new one
                newvalue = line.replace(oldvalue, value)
            else:
                # If no match, write old line without changes
                newvalue = line

            # Write result back to file
            print(newvalue, end='')

    if len(protocol) > 0:
        print('\n'.join(protocol))


def get_info(description, file, regex=None, macro=None):
    '''
    Retrieve information and return it

    :param str description: Value description
    :param File file:        File (object) to read from
    :param str regex:       Regular expression to use during read operation
    :param str macro:       Macro for read operation
    '''
    if publics.verbose >= 1:
        print('GET {}:'.format(description))

        try:
            name = file.name
        except AttributeError:
            # StringIO doesn't provide a .name attribute
            name = '(StringIO)'

        print('   file:    ' + name)
        print('   regex:   ' + (regex or ''))
        print('   macro:   ' + (macro or ''))

    result = None

    if regex:
        result = get_content(file, regex)

    if macro:
        result = get_content_by_macro(file, macro)

    if publics.verbose >= 1:
        print('   result:  ' + (result or ''))

    return result


def get_isodate():
    '''
    Just return the iso date, with some verbose prints around ...

    :return: ISO date
    :rtype: str
    '''
    if publics.verbose >= 1:
        print('GET isodate')

    result = datetime.today().strftime('%Y-%m-%d')

    if publics.verbose >= 1:
        print('   result:  ' + (result or ''))

    return result


def get_isodatetime():
    '''
    Just return the iso date and time, with some verbose prints around ...

    :return: ISO date and time
    :rtype: str
    '''
    if publics.verbose >= 1:
        print('GET isodate')

    result = datetime.today().strftime('%Y-%m-%dT%H:%M:%S')

    if publics.verbose >= 1:
        print('   result:  ' + (result or ''))

    return result


def read_file(file):
    '''
    Read (file) content into a list (each line an element)

    :param File file: File object
    :return: File content
    :rtype: List
    '''
    if publics.verbose >= 1:
        print('Read commands from {}'.format(file))

    return file.read().split('\n')


def parse_commands(content):  #pylint: disable=too-many-locals
    '''
    Read the command file and interpret its commands

    :param content: Content (patch file, line by line)
    :type content: List
    :return: commands
    :rtype: List of tupels (action, filename, regex)
    '''

    logger = logging.getLogger('parse_commands')
    logger.debug('%i lines to be parsed', len(content))

    result = []

    pp.ParserElement.setDefaultWhitespaceChars(' \t')

    action = pp.oneOf('read write replace', True).setResultsName('action')
    filename = pp.Word(pp.alphanums + r'/\_-.').setResultsName('filename')

    using = pp.oneOf('using', True)
    regex = pp.Regex('.*').setResultsName('regex')

    macrokeyword = pp.oneOf('macro', True)
    macro = pp.oneOf('vb6version', True).setResultsName('macro')

    expression = pp.oneOf('isodate isodatetime nothing').setResultsName('expression')
    cmd = pp.oneOf('get', True).setResultsName('action')
    fromkeyword = pp.oneOf('from', True)
    tokeyword = pp.oneOf('to', True)

    fileaction = action + filename + using + regex | action + filename + macrokeyword + macro
    expressionaction = cmd + expression
    replaceaction = action + filename + fromkeyword + tokeyword + using + regex

    grammar = fileaction | expressionaction | replaceaction

    lineno = 0
    for line in content:
        lineno += 1
        line = line.strip()
        if not line.startswith('#') and len(line) > 0:
            try:
                command = grammar.parseString(line)

                if publics.verbose >= 3:
                    print(command)

                if command['action'] == 'get':
                    result.append(
                        (command['action'],
                         command['expression'],
                         ''))
                elif command['action'] == 'replace':
                    result.append(
                        (command['action'],
                         command['filename'],
                         command['from'],
                         command['to'],
                         command['regex']))
                elif command['action'] == 'read':
                    try:
                        result.append(
                            ('read-using',
                             command['filename'],
                             command['regex']))
                    except KeyError:
                        pass

                    try:
                        result.append(
                            ('read-macro',
                             command['filename'],
                             command['macro']))
                    except KeyError:
                        pass
                else:
                    # Any other command
                    result.append(
                        (command['action'],
                         command['filename'],
                         command['regex']))

            except pp.ParseException as exc:
                logger.error(' Parsing error in line %i: %s',
                             lineno, exc)

    return result


def perform_commands(basefolder, commands):
    '''

    :param str basefolder: Basic folder for file operations
    :param list commands: Commands and arguments already parsed
    '''

    i = 1
    info = ''
    for command in commands:
        action = command[0]
        filename = command[1]
        expression = command[2] or filename

        if action == 'read-using':
            with open(pjoin(basefolder, filename), 'r') as file:
                info = get_info('line {}'.format(i), file, regex=expression)

        if action == 'read-macro':
            with open(pjoin(basefolder, filename), 'r') as file:
                info = get_info('line {}'.format(i), file, macro=expression)

        if action == 'write':
            destfile = pjoin(basefolder, filename)
            write_content(destfile, expression, info)

        if action == 'get':
            expression = command[1]
            if expression == 'isodate':
                info = get_isodate()
            if expression == 'isodatetime':
                info = get_isodatetime()
            if expression == 'nothing':
                info = ''

        i += 1

    # Return last info for testing
    return info


def get_basefolder(filename):
    '''
    Determine base folder from given filename.

    :param filename: Original filename information
    :return: Base folder
    :rtype: str
    '''
    basefolder = dirname(filename)
    if basefolder == '':
        basefolder = getcwd()
    if publics.verbose >= 1:
        print('Base folder: {}'.format(basefolder))
    return basefolder


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
